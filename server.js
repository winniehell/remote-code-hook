#!/usr/bin/env node

const http = require('http');
const {spawn} = require('child_process');

const config = require('./config.json');
const packageJson = require('./package.json');

const port = parseInt(process.env.PORT || 3000, 10);

console.log(`Running ${packageJson.name} on port ${port}`)
const server = http.createServer((request, response) => {
    const {method} = request;
    if (method !== 'GET') {
        return;
    }

    const {pipeline, token} = request.headers;
    const {command, args} = config[token] || {};
    if (!command) {
        console.debug(`No command found for token ${token}!`);
        return;
    }

    console.log(`Triggered by pipeline ${pipeline}`);
    console.log(`Running command "${command}" with "${(args || []).join(' ')}"`);

    let output = '';
    const {stdout, stderr} = spawn(command, args)
        .on('close', (code) => {
            response.writeHead((code === 0) ? 200 : 500, {'Content-Type': 'text/plain'});
            response.write(output);
            response.end();
        });

    stdout.on('data', (data) => {
        output += data;
    });

    stderr.on('data', (data) => {
        output += `error: ${data}`;
    });
});

server.listen(port);
